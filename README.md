##### Wiring types
* no: the default value – this means no autowiring is used for the bean and we have to explicitly name the dependencies
* byName: autowiring is done based on the name of the property, therefore Spring will look for a bean with the same name as the property that needs to be set
* byType: similar to the byName autowiring, only based on the type of the property. This means Spring will look for a bean with the same type of the property to set. If there's more than one bean of that type, the framework throws an exception.
* constructor: autowiring is done based on constructor arguments, meaning Spring will look for beans with the same type as the constructor arguments

##### Annotations
* @Configuration: Used to indicate that a class declares one or more @Bean methods. These classes are processed by the Spring container to generate bean definitions and service requests for those beans at runtime.
* @Bean: Indicates that a method produces a bean to be managed by the Spring container. This is one of the most used and important spring annotation. @Bean annotation also can be used with parameters like name, initMethod and destroyMethod.
* @PreDestroy and @PostConstruct are alternative way for bean initMethod and destroyMethod. It can be used when the bean class is defined by us.
* @ComponentScan: Configures component scanning directives for use with @Configuration classes. Here we can specify the base packages to scan for spring components.
* @Component: Indicates that an annotated class is a “component”. Such classes are considered as candidates for auto-detection when using annotation-based configuration and classpath scanning.
* @PropertySource: provides a simple declarative mechanism for adding a property source to Spring’s Environment. There is a similar annotation for adding an array of property source files i.e @PropertySources.
* @Service: Indicates that an annotated class is a “Service”. This annotation serves as a specialization of @Component, allowing for implementation classes to be autodetected through classpath scanning.
* @Repository: Indicates that an annotated class is a “Repository”. This annotation serves as a specialization of @Component and advisable to use with DAO classes.
* @Autowired: Spring @Autowired annotation is used for automatic injection of beans. Spring @Qualifier annotation is used in conjunction with Autowired to avoid confusion when we have two of more bean configured for same type.

##### Links
* https://www.baeldung.com/spring-vs-spring-boot
* https://start.spring.io/
* https://docs.spring.io/spring/docs/3.2.x/spring-framework-reference/html/beans.html
* https://dzone.com/articles/the-springbootapplication-annotation-example-in-ja
* https://www.baeldung.com/spring-autowire
* https://www.journaldev.com/16966/spring-annotations
* https://www.codingame.com/playgrounds/2096/playing-around-with-spring-bean-configuration
* https://www.baeldung.com/inversion-control-and-dependency-injection-in-spring
* https://stackoverflow.com/questions/59406908/defining-spring-beans-with-same-method-name-for-different-profiles
* https://github.com/spring-projects/spring-framework/issues/22609