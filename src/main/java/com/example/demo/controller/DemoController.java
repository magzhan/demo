package com.example.demo.controller;

import com.example.demo.entity.Demo;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.time.LocalDateTime;

@RestController
class DemoController {

    @Autowired
    private ObjectMapper objectMapper;

    @GetMapping("/1")
    public Demo first() throws JsonProcessingException {
        return Demo.builder().id("Demo").name("Demo").date(LocalDateTime.now()).build();
    }

    @GetMapping("/2")
    public String second() throws JsonProcessingException {
        return objectMapper.writeValueAsString(Demo.builder().id("Demo").name("Demo").date(LocalDateTime.now()).build());
    }
} 