package com.example.demo.configuration;

import com.example.demo.mapper.DemoMapper;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateSerializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateTimeSerializer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.converter.json.Jackson2ObjectMapperBuilder;

import java.time.format.DateTimeFormatter;

@Configuration
public class DemoConfiguration {

    @Bean
    public ObjectMapper objectMapper() {
        Jackson2ObjectMapperBuilder builder = new Jackson2ObjectMapperBuilder();
        builder.serializers(new LocalDateSerializer(DateTimeFormatter.ofPattern("yyyy/MM/dd")));
        builder.serializers(new LocalDateTimeSerializer(DateTimeFormatter.ofPattern("yyyy/MM/dd")));
        return builder.build();
    }

    @Bean
    public ObjectMapper objectMapper(Jackson2ObjectMapperBuilder builder) {
        builder.serializers(new LocalDateSerializer(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss")));
        builder.serializers(new LocalDateTimeSerializer(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss")));
        return builder.build();
    }

    @Bean
    public ObjectMapper objectMapper(ObjectMapper objectMapper) {
        Jackson2ObjectMapperBuilder builder = new Jackson2ObjectMapperBuilder();
        builder.serializers(new LocalDateSerializer(DateTimeFormatter.ofPattern("yyyy:MM:dd")));
        builder.serializers(new LocalDateTimeSerializer(DateTimeFormatter.ofPattern("yyyy:MM:dd")));
        return builder.build();
    }

    @Bean(name = "objectMapperBest")
    public ObjectMapper objectMapperBest() {
        Jackson2ObjectMapperBuilder builder = new Jackson2ObjectMapperBuilder();
        builder.serializers(new LocalDateSerializer(DateTimeFormatter.ofPattern("yyyy*MM*dd")));
        builder.serializers(new LocalDateTimeSerializer(DateTimeFormatter.ofPattern("yyyy*MM*dd")));
        return builder.build();
    }

    @Bean
    public DemoMapper objectDemoMapperBest() {
        Jackson2ObjectMapperBuilder builder = new Jackson2ObjectMapperBuilder();
        builder.serializers(new LocalDateSerializer(DateTimeFormatter.ofPattern("yyyy+MM+dd")));
        builder.serializers(new LocalDateTimeSerializer(DateTimeFormatter.ofPattern("yyyy+MM+dd")));
        return new DemoMapper(builder.build());
    }

}
