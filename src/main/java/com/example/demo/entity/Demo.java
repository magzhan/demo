package com.example.demo.entity;

import lombok.Builder;
import lombok.Data;

import java.time.LocalDateTime;

@Data
@Builder
public class Demo {
    private String id;
    private String name;
    private LocalDateTime date;
}
